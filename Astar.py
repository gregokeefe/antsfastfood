import unittest
import fastfood
import sys
import StringIO
import os
import sys
import heapq


class Cell(object):
    def __init__(self, r, c, walkable):
        self.walkable = walkable
        self.c = c
        self.r = r
        self.parent = None
        self.g = 0
        self.h = 0
        self.f = 0


class AStar(object):
    def __init__(self):
        self.opened = []
        heapq.heapify(self.opened)
        self.closed = set()
        self.cells = []
        self.path = []

    def set_up(self):
        self.bot = fastfood.Ants()

        # instantiate bot values
        self.bot.width = 10
        self.bot.height = 10
        self.bot.turnTime = 10
        self.bot.loadTime = 10
        self.bot.viewRadius2 = 2
        self.bot.attackRadius2 = 2
        self.bot.spawnRadius2 = 2
        self.bot.myAnts = [(0, 0)]
        self.bot.myHills = [(1, 0)]
        self.bot.enemyAnts = [(4, 4)]
        self.bot.enemyHills = [(9, 4)]
        self.bot.food = [(0, 3)]
        self.bot.territory = [[0 for r in range(self.bot.height)] for c in range(self.bot.width)]
        self.bot.value = [[1 for c in range(self.bot.width)] for r in range(self.bot.height)]
        self.obstacle = [(1, 1), (4, 1), (2, 2), (3, 2), (2, 3), (0, 5), (1, 5), (3, 5), (5, 0)]
        # self.obstacle = [(1, 1), (3, 1), (4, 1), (5, 1), (3, 2), (2, 3), (0, 5), (1, 5), (3, 5)]
        for r in range(0, self.bot.width):
            for c in range(0, self.bot.height):
                self.bot.territory[r][c] = fastfood.LAND
                if(r, c) in self.obstacle:
                    print(r, c)
                    walkable = False
                elif(r, c) in self.bot.myHills:
                    walkable = False
                elif(r, c) in self.bot.enemyAnts:
                    walkable = False
                else:
                    walkable = True
                self.cells.append(Cell(r, c, walkable))

        for i in self.obstacle:
            self.bot.territory[i[0]][i[1]] = fastfood.WATER
        for i in self.bot.enemyHills:
            self.goal = self.get_tile(i[0], i[1])
            self.bot.territory[i[0]][i[1]] = fastfood.ENEMY_HILL
        for i in self.bot.myAnts:
            self.start = self.get_tile(i[0], i[1])
            self.bot.territory[i[0]][i[1]] = fastfood.MY_ANT
        for i in self.bot.myHills:
            self.bot.territory[i[0]][i[1]] = fastfood.MY_HILL
        for i in self.bot.enemyAnts:
            self.bot.territory[i[0]][i[1]] = fastfood.ENEMY_ANT
        for i in self.bot.enemyAnts:
            self.bot.territory[i[0]][i[1]] = fastfood.FOOD

    def get_heuristic(self, cell):
        return 10 * (abs(cell.r - self.goal.r) + abs(cell.c - self.goal.c))

    def get_tile(self, r, c):
        return self.cells[r * self.bot.height + c]

    def get_adjacent_tiles(self, cell):
        cells = []
        if cell.r < self.bot.width - 1:
            cells.append(self.get_tile(cell.r + 1, cell.c))
        if cell.c > 0:
            cells.append(self.get_tile(cell.r, cell.c - 1))
        if cell.r > 0:
            cells.append(self.get_tile(cell.r - 1, cell.c))
        if cell.c < self.bot.height - 1:
            cells.append(self.get_tile(cell.r, cell.c + 1))
        return cells

    def display_path(self):
        cell = self.goal
        while cell.parent is not self.start:
            cell = cell.parent
            self.path.append((cell.r, cell.c))
            print('path:cell: %d, %d' % (cell.r, cell.c))
        self.display_map()

    def update_cell(self, adj, cell):
        adj.g = cell.g + 10
        adj.h = self.get_heuristic(adj)
        adj.parent = cell
        adj.f = adj.h + adj.g

    def find_path(self):
        heapq.heappush(self.opened, (self.start.f, self.start))
        while len(self.opened):
            f, cell = heapq.heappop(self.opened)
            self.closed.add(cell)
            if cell is self.goal:
                self.display_path()
                return
            adj_tiles = self.get_adjacent_tiles(cell)
            for adj_tile in adj_tiles:
                if adj_tile.walkable and adj_tile not in self.closed:
                    if(adj_tile.f, adj_tile) in self.opened:
                        if adj_tile.g > cell.g + 10:
                            self.update_cell(adj_tile, cell)
                    else:
                        self.update_cell(adj_tile, cell)
                        heapq.heappush(self.opened, (adj_tile.f, adj_tile))

    def display_map(self):
        s = ""
        for r in range(0, self.bot.width):
            for c in range(0, self.bot.height):
                if(r, c) in self.obstacle:
                    s += '[#]'.__str__()
                elif(r, c) in self.bot.myAnts:
                    s += '[A]'.__str__()
                elif(r, c) in self.bot.enemyHills:
                    s += '[H]'.__str__()
                elif(r, c) in self.path:
                    s += '[.]'.__str__()
                elif(r, c) in self.bot.enemyAnts:
                    s += '[E]'.__str__()
                elif(r, c) in self.bot.myHills:
                    s += '[h]'.__str__()
                elif(r, c) in self.bot.food:
                    s += '[F]'.__str__()
                else:
                    s += '[ ]'.__str__()
            s += "\n"
        print s

run = AStar()
run.set_up()
run.display_map()
run.find_path()
