import unittest
import fastfood
import sys
import StringIO


class FastFoodTests(unittest.TestCase):
    def setUp(self):
        self.bot = fastfood.Ants()
        # self.originalIN = sys.stdout
        # self.input = StringIO.StringIO()
        # sys.stdout = self.input

        # instantiate bot values
        self.bot.width = 10
        self.bot.height = 10
        self.bot.turnTime = 10
        self.bot.loadTime = 10
        self.bot.viewRadius2 = 2
        self.bot.attackRadius2 = 2
        self.bot.spawnRadius2 = 2

        self.bot.myAnts = [(4, 6), (0, 9)]
        self.bot.myHills = [(4, 8)]
        self.bot.enemyAnts = [(0, 4)]
        self.bot.territory = [[0 for r in range(self.bot.height)] for c in range(self.bot.width)]
        self.bot.value = [[1 for c in range(self.bot.width)] for r in range(self.bot.height)]
        s = ""
        for r in range(0, self.bot.width):
            for c in range(0, self.bot.height):
                if r == 3:
                    self.bot.territory[r][c] = fastfood.WATER
                else:
                    self.bot.territory[r][c] = fastfood.LAND
                s += self.bot.territory[r][c].__str__()
            s += "\n"
        # print s

    def tearDown(self):
        return
        # sys.stdout = self.originalIN

    def test_neighbours(self):
        """Tests that the neighbours method returns the correct neighbours for a square"""
        print "testing neighbours"
        r = 4
        c = 3
        neigh = self.bot.neighbours(r, c)
        correct_values = fastfood.AIM.values()
        is_correct = True
        for i in range(len(neigh)):
            if neigh[i] != ((r + correct_values[i][0]) % self.bot.width, (c + correct_values[i][1]) % self.bot.height):
                is_correct = False
            # print neigh[i][0].__str__() + ", " + neigh[i][1].__str__()
        self.assertTrue(is_correct)

    def test_neighbours_wrapping(self):
        """Tests that the neighbours method returns the correct
        neighbours for a square which is on the edge of the map"""
        print "testing neighbours wrapping"
        r = 0
        c = 0
        neigh = self.bot.neighbours(r, c)
        correct_values = fastfood.AIM.values()
        is_correct = True
        for i in range(len(neigh)):
            if neigh[i] != ((r + correct_values[i][0]) % self.bot.width, (c + correct_values[i][1]) % self.bot.height):
                is_correct = False
                # print neigh[i][0].__str__() + ", " + neigh[i][1].__str__()
        self.assertTrue(is_correct)

    def test_move_dest(self):
        """Tests that move_dest method returns the correct row/column adjustment for each direction"""
        for r, c in self.bot.myAnts:
            self.bot.move(r, c, 'n')

        is_correct = True
        for i in range(len(self.bot.myAntsNext)):
            if self.bot.myAntsNext[i] != ((self.bot.myAnts[i][0] - 1) % self.bot.height, self.bot.myAnts[i][1]):
                is_correct = False
        self.assertTrue(is_correct)

    def test_move_output(self):
        """Tests that move method outputs the correct string for each direction"""
        original_out = sys.stdout
        output = StringIO.StringIO()
        sys.stdout = output

        for r, c in self.bot.myAnts:
            self.bot.move(r, c, 'n')

        is_correct = True
        line = sys.stdout.getvalue()
        if line != "o 4 6 n\no 0 9 n\n":
            is_correct = False

        sys.stdout = original_out
        self.assertTrue(is_correct)

    def test_update_ants(self):
        """Tests that ally and enemy ant locations are read from the engine and updated correctly"""
        input_string = "a 4 6 0\n"
        input_string += "a 0 9 0\n"
        input_string += "a 7 2 1\n"
        input_string += "a 0 0 0\n"
        input_string += "a 2 2 2\n"
        self.bot.update(input_string)

        expmyants = [(4, 6), (0, 9), (0, 0)]
        expenemyants = [(7, 2), (2, 2)]

        is_correct = True
        for i in range(len(expmyants)):
            if expmyants[i] != self.bot.myAnts[i]:
                is_correct = False
        for i in range(len(expenemyants)):
            if expenemyants[i] != self.bot.enemyAnts[i]:
                is_correct = False
        self.assertTrue(is_correct)

    def test_update_hills(self):
        """Tests that ally and enemy hills are correctly updated each turn"""
        self.bot.enemyHills = [(5, 5)]
        self.bot.myHills = []
        input_string = "h 2 2 0\n"
        input_string += "h 5 5 1\n"
        input_string += "h 7 3 1\n"
        input_string += "h 9 9 0\n"
        self.bot.update(input_string)
        expmyhills = [(2, 2), (9, 9)]
        expenemyhills = [(5, 5), (7, 3)]

        is_correct = True
        for i in range(len(expmyhills)):
            if expmyhills[i] != self.bot.myHills[i]:
                is_correct = False
        for i in range(len(expenemyhills)):
            if expenemyhills[i] != self.bot.enemyHills[i]:
                is_correct = False
        self.assertTrue(is_correct)

    def test_update_land(self):
        """Tests that the update method correctly reads food and water input from the engine"""
        expwater = self.bot.territory
        input_string = "f 2 2\n"
        input_string += "w 5 5\n"
        input_string += "f 7 3\n"
        input_string += "w 9 9\n"
        self.bot.update(input_string)
        expfood = [(2, 2), (7, 3)]
        expwater[5][5] = fastfood.WATER
        expwater[9][9] = fastfood.WATER

        is_correct = True
        for i in range(len(expfood)):
            if expfood[i] != self.bot.food[i]:
                is_correct = False
        for i in range(len(expwater)):
            for j in range(len(expwater[i])):
                if expwater[i][j] != self.bot.territory[i][j]:
                    is_correct = False
        self.assertTrue(is_correct)

    def test_setup(self):
        """Test that the AI correctly reads the initial input."""
        is_correct = True
        self.bot.setup("attackradius2 8\n")
        if self.bot.attackRadius2 != 8:
            is_correct = False

        self.assertTrue(is_correct)

    def test_do_turn(self):
        """Test do_turn method processes all ants that the player has."""
        print "testing do_turn"
        is_correct = True
        for r, c in self.bot.myAnts:
            self.bot.move(r, c, 'x')
        if int(len(self.bot.myAntsNext)) != int(len(self.bot.myAnts)):
            is_correct = False
        else:
            for i in range(len(self.bot.myAntsNext)):
                if self.bot.myAntsNext[i] != ((self.bot.myAnts[i][0]) % self.bot.height, self.bot.myAnts[i][1]):
                    is_correct = False

        self.assertTrue(is_correct)

    def test_dir_value_water(self):
        """tests that dir_value returns -99999 for an ant movement that would result in the ant going into water."""
        print "testing dir_value_water"
        is_correct = True
        # print(self.bot.dir_value(4, 6, 'n'))
        if self.bot.dir_value(4, 6, 'n') != -99999:
            is_correct = False

        self.assertTrue(is_correct)

    def test_dir_value_normal(self):
        """tests that dir_value returns a correct value for a tile that is not water"""
        print "testing dir_value_normal"
        is_correct = True
        # print(int(self.bot.dir_value(4, 6, 'e')))
        if self.bot.dir_value(4, 6, 'e') == -99999:
            is_correct = False

        self.assertTrue(is_correct)

if __name__ == '__main__':
    unittest.main()
