#!/usr/bin/env python

"""
Extend the fast food bot visualiser with human input to increase the value around clicked tile.

Greg, February 2016

"""

import visualise
import sys
import pygame


class AntsHuman(visualise.AntsViz):

    def __init__(self):
        super(AntsHuman, self).__init__()
        self.input_human_value = [[]]

    def hook_post_setup(self):
        """Set up an array for human input values."""
        self.input_human_value = [[0 for c in range(self.width)] for r in range(self.height)]
        super(AntsHuman, self).hook_post_setup()

    def handle_event(self, event):
        """Respond to user clicks by updating input value array."""
        if event.type == pygame.MOUSEBUTTONDOWN:
            tile_row, tile_col = self.tile_clicked(event)
            # self.show_message("tile (%s, %s) clicked" % (tile_row, tile_col) )
            if 0 <= tile_row < self.height and 0 <= tile_col < self.width:
                # self.show_message("farnarkling tile (%s, %s)" % (tile_row, tile_col) )
                self.input_human_value[tile_row][tile_col] = 11
            else:
                # self.show_message("error %s %s %s %s" % \
                # `                 (tile_row, self.height, tile_col, self.width))
                pass

        else:
            super(AntsHuman, self).handle_event(event)

    # determine which tile mouse has been clicked on
    def tile_clicked(self, event):
        pixel_col, pixel_row = event.pos
        return ((pixel_row - self.viz_map_origin_y) / self.viz_tile_size,
                (pixel_col - self.viz_map_origin_x) / self.viz_tile_size)

    def hook_post_events(self):
        """Apply human player input to tile values, and discount."""
        for r in range(self.height):
            for c in range(self.width):
                if self.input_human_value[r][c] > 1:
                    # self.show_message("adding human value %s at (%s, %s)" % \
                    #                  (self.input_human_value[r][c], r, c) )
                    self.add_value(r, c, self.input_human_value[r][c], 0.1)
                    self.input_human_value[r][c] *= 0.9
                else:
                    self.input_human_value[r][c] = 0


# main (doesn't get run if we are imported as a module)
if __name__ == "__main__":
    AntsHuman().run()
