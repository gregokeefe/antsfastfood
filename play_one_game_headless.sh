#!/usr/bin/env sh
./tools/playgame.py \
    --turns 1000  \
    --nolaunch \
    --loadtime 10000 \
    --turntime 10000 \
    --log_stderr \
    --player_seed 42 \
    --engine_seed 11 \
    --end_wait .05 \
    --verbose \
    --fill \
    --log_dir game_logs \
    --map_file tools/maps/cell_maze/cell_maze_p02_04.map \
    "$@"  \
    "python fastfood.py" \
    "python tools/sample_bots/python/GreedyBot.py"
$SHELL

#    "& java -jar /home/pi/code/antsplayer/game/MyBot.jar" 
#    --map_file tools/maps/cell_maze/cell_maze_p02_06.map \
#    --map_file tools/maps/maze/maze_p04_33.map \
#    --map_file tools/maps/cell_maze/cell_maze_p04_05.map \
#    --map_file tools/maps/cell_maze/cell_maze_p04_12.map \
#    --map_file tools/maps/cell_maze/cell_maze_p04_22.map \
#    --map_file tools/maps/maze/maze_p06_05.map \
#    --map_file tools/maps/example/tutorial1.map \
