#!/usr/bin/env python

import cProfile
import fastfood
    
cProfile.run('fastfood.Ants().run()', 'fastfood.profile')
