"""
Reads an Ants game replay file, and converts it to bot input, so that a game state visualising bot can be used to visualise a game replay.

Handles revision 3, which is different to revision 2 as documented at ants.aichallenge.org.

Greg, March 2016
"""

import json
import sys

AIM = {'n': (-1, 0), 'e': (0, 1), 's': (1, 0), 'w': (0, -1), '-': (0, 0)}

# parse replay file
if len(sys.argv) == 2:
    filename = sys.argv[1]
else:
    print("Ants replay converter")
    print("exactly one argument required: path to replay file to be converted.")

game_data = open(filename)
game = json.load(game_data)


# turns, start "0th turn"
winning_turn = game['replaydata']['winning_turn'] 
turn = -1

def next_turn():
    global turn
    # complete current turn, if there is one
    if turn == 0:
        print("ready")
    elif turn > 0:
        print("go")
    # start next turn, if there is one
    turn += 1
    if turn <= winning_turn:
        print("turn %s" % turn)

next_turn()


# game parameter info
rows = game['replaydata']['map']['rows']
cols = game['replaydata']['map']['cols']

print("rows %s" % rows)
print("cols %s" % cols)

def game_parm(parm):
    print("%s %s" %  (parm, game['replaydata'][parm]))

map(game_parm, ['player_seed', 'turntime', 'spawnradius2', 'viewradius2',
                'loadtime', 'turns', 'attackradius2', 'players'])

next_turn()


# all water from map goes out in first turn
map = game['replaydata']['map']['data']
for r in range(rows):
    for c in range(cols):
        if map[r][c] == '%':
            print("w %s %s" % (r,c))

# we must track the position of ants by applying their moves,
# so here is initial (row, col) for each ant
antlocs = [ tuple(ant[:2]) for ant in game['replaydata']['ants'] ]

def dest(loc, dir):
    row, col = loc
    dr, dc = AIM[dir]
    r = (row + dr) % rows
    c = (col + dc) % cols
    return (r, c)

# ant, hill and food turns
while turn <= winning_turn:
    for hill in game['replaydata']['hills']:
        if turn <= hill[3]:
            print("h %s %s %s" % tuple(hill[:3]))
    for food in game['replaydata']['food']:
        if turn >= food[2] and turn <= food[3]:
            print("f %s %s" % tuple(food[:2]))
    # ants are a little tricky as we must track their position from their moves
    for antnum in range(len(antlocs)):
        ant = game['replaydata']['ants'][antnum]
        if turn >= ant[2] and turn < ant[3]:
            print("a %s %s %s" % (antlocs[antnum] + (ant[4],)))
            if turn < winning_turn:
                dir = ant[5][turn - ant[2]]
                antlocs[antnum] = dest(antlocs[antnum], dir)
    next_turn()



