# To build, run ./setup.sh (or python setup.py build_ext --inplace)
# Then you should be able to import antplaylib module as normal

from setuptools import setup
from Cython.Build import cythonize

setup(
    name='Python Fast Food Ants Bot',
    ext_modules=cythonize("fastfood.py"),
)
