#!/usr/bin/env python

"""
Extend the fast food bot with game state visualisation, using pygame.

Greg, February 2016

"""

import fastfood
from fastfood import SEEN, UNSEEN, SEEING, WATER, MY_HILL, ENEMY_HILL, FOOD, MY_ANT, ENEMY_ANT
import sys
import Tkinter as Tkt
from Tkinter import *
import tkFont
import pygame
import os
import platform
import colorsys


class AntsViz(fastfood.Ants):

    def __init__(self):
        super(AntsViz, self).__init__()

        # map colors to tile types (use a dictionary instead!)
        self.white = (255, 255, 255)
        self.viz_seen = (70, 70, 70)  # (100, 100, 100)
        self.viz_unseen = (230, 230, 230)  # (90, 70, 70)
        self.viz_seeing = (150, 90, 70)
        self.viz_water = (0, 80, 20)
        # self.viz_water = (0, 30, 255)
        self.viz_me = (20, 50, 250)
        self.viz_enemy = (250, 50, 20)
        self.viz_w = 720
        self.viz_h = 720
        self.viz_tile_size = 0
        self.viz_map_origin_x = 0
        self.viz_map_origin_y = 0
        self.viz_clock = pygame.time.Clock()
        self.font = None
        self.blank_msg = None
        self.viz_tiles = {}

        # TEST CODE

        self.root = Tkt.Tk(screenName="Ants Game")
        self.root.maxsize(1280, 720)
        self.root.minsize(1280, 720)
        self.root.rowconfigure(0, weight=1)
        self.root.columnconfigure(0, weight=1)
        embed = Tkt.Frame(self.root, width=720, height=720)  # creates embed frame for pygame window
        embed.grid(columnspan=720, rowspan=720)  # Adds grid
        embed.pack(side=LEFT)  # packs window to the left
        self.controlframe = Tkt.Frame(self.root, width=560, height=720)
        self.controlframe.grid(columnspan=560, rowspan=720)
        self.controlframe.pack(side=TOP)
        os.environ['SDL_WINDOWID'] = str(embed.winfo_id())
        if platform.system == "Windows":
            os.environ['SDL_VIDEODRIVER'] = 'windib'
        self.viz_screen = pygame.display.set_mode((720, 720))
        self.viz_background = pygame.Surface(self.viz_screen.get_size())

        self.scale1 = Scale(self.controlframe, from_=-10, to=10, length=500, tickinterval=2, orient=HORIZONTAL)
        self.scale1.set(self.foodValue)
        # self.scale2 = Scale(self.controlframe, from_=-10, to=10, length=500, tickinterval=2, orient=HORIZONTAL)
        # self.scale2.set(self.unexploredValue)
        self.scale3 = Scale(self.controlframe, from_=-10, to=10, length=500, tickinterval=2, orient=HORIZONTAL)
        self.scale3.set(self.myHillsValue)
        # self.scale4 = Scale(self.controlframe, from_=-10, to=10, length=500, tickinterval=2, orient=HORIZONTAL)
        # self.scale4.set(self.enemyAntsValue)
        self.scale5 = Scale(self.controlframe, from_=-10, to=10, length=500, tickinterval=2, orient=HORIZONTAL)
        self.scale5.set(self.enemyHillsValue)

        self.scale1.bind("<ButtonRelease-1>", self.scale1event)
        # self.scale2.bind("<ButtonRelease-1>", self.scale2event)
        self.scale3.bind("<ButtonRelease-1>", self.scale3event)
        # self.scale4.bind("<ButtonRelease-1>", self.scale4event)
        self.scale5.bind("<ButtonRelease-1>", self.scale5event)

        titlefont = tkFont.Font(family="Helvetica Standard", underline=1, weight="bold")
        headingfont = tkFont.Font(family="Helvetica Standard")
        label0 = Label(self.controlframe, text="Objective Priorities", font=titlefont, pady=10)
        label1 = Label(self.controlframe, text="Food", font=headingfont, height=2, anchor="s")
        # label2 = Label(self.controlframe, text="Unexplored Tiles", font=headingfont, height=2, anchor="s")
        label3 = Label(self.controlframe, text="Allied Hills", font=headingfont, height=2, anchor="s")
        # label4 = Label(self.controlframe, text="Enemy Ants", font=headingfont, height=2, anchor="s")
        label5 = Label(self.controlframe, text="Enemy Hills", font=headingfont, height=2, anchor="s")

        label0.pack(side=TOP)
        label1.pack(side=TOP)
        self.scale1.pack(side=TOP)
        # label2.pack(side=TOP)
        # self.scale2.pack(side=TOP)
        label3.pack(side=TOP)
        self.scale3.pack(side=TOP)
        # label4.pack(side=TOP)
        # self.scale4.pack(side=TOP)
        label5.pack(side=TOP)
        self.scale5.pack(side=TOP)

        pygame.display.init()
        pygame.display.update()

    def scale1event(self, event):
        self.foodValue = self.scale1.get()

    def scale2event(self, event):
        pass

    def scale3event(self, event):
        self.myHillsValue = self.scale3.get()

    def scale4event(self, event):
        pass
        # self.enemyAntsValue = self.scale4.get()

    def scale5event(self, event):
        self.enemyHillsValue = self.scale5.get()

    def hook_post_setup(self):
        """Set up pygame to visualise the game as it happens."""

        pygame.init()
        # self.viz_screen = pygame.display.set_mode((500, 500), pygame.RESIZABLE)
        display_info = pygame.display.Info()
        self.viz_w = display_info.current_w
        self.viz_h = display_info.current_h
        self.viz_tile_size = min([self.viz_w / self.width, self.viz_h / self.height])

        self.viz_clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, 20)

        self.viz_map_origin_x = (self.viz_w - self.viz_tile_size * self.width) / 2
        self.viz_map_origin_y = (self.viz_h - self.viz_tile_size * self.height) / 2

        self.viz_clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, 20)
        self.blank_msg = pygame.Surface((self.viz_w, self.viz_map_origin_y))
        self.blank_msg.fill((0, 0, 0))

        # display some start-up guff
        self.show_message("Ants for Humans, let's go!")
        if False:
            msg_txt = "screen size (%s, %s); " % (self.viz_w, self.viz_h) + \
                      "map origin (%s, %s); " % \
                      (self.viz_map_origin_x, self.viz_map_origin_y) + \
                      "map size in tiles (%s, %s)" % (self.width, self.height)
            self.show_message(msg_txt, pause=5)

        # pygame.display.set_caption('Ants Bot Runtime Visualisation')

        self.viz_background = pygame.Surface(self.viz_screen.get_size())
        self.viz_background = self.viz_background.convert()

        # this dictionary of tiles may make more sense when we use more images
        self.viz_tiles = {SEEN: pygame.Surface((self.viz_tile_size, self.viz_tile_size))}
        self.viz_tiles[SEEN].fill(self.viz_seen)
        self.viz_tiles[UNSEEN] = pygame.Surface((self.viz_tile_size, self.viz_tile_size))
        self.viz_tiles[UNSEEN].fill(self.viz_unseen)
        self.viz_tiles[WATER] = pygame.Surface((self.viz_tile_size, self.viz_tile_size))
        self.viz_tiles[WATER].fill(self.viz_water)
        self.viz_tiles[MY_HILL] = pygame.Surface((self.viz_tile_size, self.viz_tile_size))
        self.viz_tiles[MY_HILL].fill(self.viz_me)
        self.viz_tiles[ENEMY_HILL] = pygame.Surface((self.viz_tile_size, self.viz_tile_size))
        self.viz_tiles[ENEMY_HILL].fill(self.viz_enemy)

        self.viz_tiles[FOOD] = pygame.image.load("food_25x25.png")
        self.viz_tiles[MY_ANT] = pygame.image.load("ant_25x25_blue.png")
        self.viz_tiles[ENEMY_ANT] = pygame.image.load("ant_25x25_red.png")

        tile_size_pair = (self.viz_tile_size, self.viz_tile_size)
        for key, tile in self.viz_tiles.items():
            # self.viz_tiles[key] = tile.convert(32)
            tile = tile.convert_alpha()
            self.viz_tiles[key] = pygame.transform.smoothscale(tile, tile_size_pair)

        for r in range(self.height):
            for c in range(self.width):
                self.viz_background.blit(self.viz_tiles[UNSEEN], self.viz_tile_loc(r, c))

        self.root.update()
        self.root.after(0, pygame.display.update())

    def show_message(self, msg_txt, pause=1):
        mw, mh = self.font.size(msg_txt)
        txt_surface = self.font.render(msg_txt, True, self.white)
        self.viz_screen.blit(self.blank_msg, (0, 0))
        self.viz_screen.blit(txt_surface, ((self.viz_w - mw) / 2, 1))
        pygame.display.flip()
        self.viz_clock.tick(1.0 / pause)

    def hook_post_value(self):
        """Handle user input and display game state visualisation."""

        if self.gameOver:
            self.show_message("Game Over!", pause=3)
            # and more post-game debrief

        for event in pygame.event.get():
            self.handle_event(event)
        self.hook_post_events()
        self.paint_tile_values()
        self.draw_game_objects()

        self.root.update()
        self.root.update_idletasks()

        self.viz_screen.blit(self.viz_background, (0, 0))
        pygame.display.flip()
        self.viz_clock.tick(20)

    def handle_event(self, event):
        """End game if user hits escape."""
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            self.show_message("escape, bye!", pause=3)
            self.playing = False

    def hook_post_events(self):
        """Chance for interactive subclasses to finalise user input processing."""
        pass

    def paint_tile_values(self):
        """Have tiles brightness indicate their value."""

        # this is a pathetic hack, find some way to have bot provide
        # min and max possible tile values
        minval = -25
        maxval = 40

        def valcolor(val):
            """Colour for display of the given tile value."""
            if val > maxval:
                # sys.stderr.write("val %s, minval %s, maxval %s\n" % (val, minval, maxval))
                # return False
                val = maxval
            elif val < minval:
                val = minval

            # else:
            normed = 255.0 * (val - minval) / (maxval - minval)
            return normed, 3 * normed / 5, normed / 2  # , 250

        # paint tiles: known water, or tile value
        tempsurface = pygame.Surface((self.viz_tile_size, self.viz_tile_size))
        for r in range(self.height):
            for c in range(self.width):
                if self.territory[r][c] == WATER:
                    self.viz_background.blit(self.viz_tiles[WATER], self.viz_tile_loc(r, c))
                else:
                    col = valcolor(self.value[r][c])
                    if self.territory[r][c] == UNSEEN:
                        pass
                        # self.viz_background.blit(self.viz_tiles[UNSEEN], self.viz_tile_loc(r, c))
                    elif col:
                        # self.viz_tiles[SEEN].fill(col)

                        if self.territory[r][c] == SEEING:

                            """
                            col = colorsys.rgb_to_hls(col[0], col[1], col[2])
                            col = (col[0], 1.25*col[1], col[2])
                            while col[1] > 100:
                                col = (col[0], col[1]-2, col[2])
                            if col[1] < 0:
                                col = (col[0], 0, col[2])
                            col = colorsys.hls_to_rgb(col[0], col[1], col[2])
                            """
                            col = (col[0] * 1.25, col[1] * 1.25, col[2] * 1.25)
                        if col[0] < 0:
                            col = (0, col[1], col[2])
                        if col[1] < 0:
                            col = (col[0], 0, col[2])
                        if col[2] < 0:
                            col = (col[0], col[1], 0)

                        if col[0] > 255:
                            col = (255, col[1], col[2])
                        if col[1] > 255:
                            col = (col[0], 255, col[2])
                        if col[2] > 255:
                            col = (col[0], col[1], 2550)
                        # sys.stderr.write("tilecol  " + str(col[0]) + ", " + str(col[1]) + ", " + str(col[2]) + "\n")
                        tempsurface.fill(col)
                        self.viz_background.blit(tempsurface, self.viz_tile_loc(r, c))
                    else:
                        sys.stderr.write("turn %s, r %s, c %s\n" % (self.turn, r, c))

    def draw_game_objects(self):
        """Draw ants, hills and food."""
        for r, c in self.myAnts:
            self.viz_background.blit(self.viz_tiles[MY_ANT], self.viz_tile_loc(r, c))
        for r, c in self.enemyAnts:
            self.viz_background.blit(self.viz_tiles[ENEMY_ANT], self.viz_tile_loc(r, c))
        for r, c in self.myHills:
            self.viz_background.blit(self.viz_tiles[MY_HILL], self.viz_tile_loc(r, c))
        for r, c in self.enemyHills:
            self.viz_background.blit(self.viz_tiles[ENEMY_HILL], self.viz_tile_loc(r, c))
        for r, c in self.food:
            self.viz_background.blit(self.viz_tiles[FOOD], self.viz_tile_loc(r, c))

    def viz_tile_loc(self, r, c):
        return (self.viz_map_origin_x + c * self.viz_tile_size,
                self.viz_map_origin_y + r * self.viz_tile_size)

# main (doesn't get run if we are imported as a module)
if __name__ == "__main__":
    AntsViz().run()
